\documentclass[11pt]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}
\usepackage{wrapfig}
\usepackage{natbib}
%\usepackage[square,comma,numbers,sort]{natbib}
\usepackage[pdftex, plainpages=false, colorlinks=true, linkcolor=blue, citecolor=blue, bookmarks=false,urlcolor=blue]{hyperref}
\usepackage{setspace}
\usepackage{multicol}
\usepackage{sectsty}
\usepackage{url}
\usepackage{lipsum}
\usepackage{times}
\usepackage[tiny,compact]{titlesec}
\usepackage{fancyhdr}
\usepackage{deluxetable}
\usepackage[font=footnotesize,labelfont=bf]{caption}
\usepackage{verbatim}
\usepackage{acronym}

\setlength{\textwidth}{6.5in}
\setlength{\oddsidemargin}{0.0cm}
\setlength{\evensidemargin}{0.0cm}
\setlength{\topmargin}{-0.5in}
\setlength{\headheight}{0.2in}
\setlength{\headsep}{0.2in}
\setlength{\textheight}{9.in}
%\setlength{\footskip}{-0.2in}
%\setlength{\voffset}{0.0in}


\sectionfont{\normalsize}
\subsectionfont{\normalsize}
\subsubsectionfont{\normalsize}
\singlespacing

\pagestyle{fancy}
\fancyhf{} 
\rhead{\fancyplain{}{ PI: S.M. Couch }}
\lhead{\fancyplain{}{DE-FOA-0001386: The CCSN Sensitivity Machine}}
\rfoot{\fancyplain{}{\thepage}}

\bibliographystyle{apj}
%\bibliographystyle{hapj}
%\bibliographystyle{physrev}
\input journal_abbr.tex

%\titlespacing*{\section}{0in}{0.2in}{0in}
%\titlespacing*{\subsection}{0in}{0.1in}{0in}
\titleformat*{\subsection}{\itshape}
%\titlespacing*{\subsubsection}{0in}{0.in}{0in}
\titleformat*{\subsubsection}{\itshape}
\setlength{\abovecaptionskip}{3pt}

\begin{document}
\thispagestyle{plain}

\begin{center}
  {\bf The Core-collapse Supernova Sensitivity Machine} \\
  Sean M. Couch, Assistant Professor \\
  Michigan State University \\
  517-884-5035, \href{mailto:couch@pa.msu.edu}{couch@pa.msu.edu} \\
  Year Doctorate Awarded: 2010 \\
  Number of Times Previously Applied: 0 \\
  Funding Opportunity Announcement Number: {\bf DE-FOA-0001386}\\
\end{center}

Core-collapse supernovae (CCSNe) are the most extreme laboratories for nuclear physics in the universe.
Stellar core collapse and the violent explosions that follow give birth to neutron stars and black holes, and in the process synthesize most of the elements heavier than helium throughout the universe.
The behavior of matter at supranuclear densities is crucial to the CCSN mechanism, as are strong and weak interactions.
Beyond Standard Model behavior of neutrinos may also impact the CCSN mechanism.
Despite the key role CCSNe play in many aspects of astrophysics, and decades of research effort, {\it we still do not understand the details of the physical mechanism that causes these explosions.}
This leaves frustratingly large error bars on many key aspects of our theoretical understanding of the universe, and also makes it difficult to constrain uncertain nuclear physics with data from CCSNe.

%Despite over a half-century of theoretical and computational effort, the details of the mechanism that reverses stellar core collapse and drives robust CCSN explosions remain uncertain.
%The most promising candidate for the explosion mechanism is the so-called ``delayed neutrino heating'' mechanism, wherein a small fraction of the energy carried away from the newborn neutron star by neutrinos is trapped by the hot, dense matter behind the stalled shock.
%This is a challenge to achieve in practice since the cross sections for neutrino interactions are tiny and there exist built-in feedbacks in the CCSN problem that tend to make neutrino-driven explosions marginal at best.
%The neutrino mechanism generally fails in 1D for all but the lowest-mass progenitors.
%The outlook for the neutrino mechanism is more promising in 2D \citep{Muller:2012a, Bruenn:2014} and 3D \citep{Melson:2015}, though there is still broad disagreement in both quantitative and qualitative results of 2D simulations \citep[see, e.g.,][]{Dolence:2015} and whether or not success in 2D will translate to success in 3D \citep{Tamborra:2014, Couch:2014}.
%The lack of certainty in the explosion mechanism...

%A major challenge in the pursuit of a successful CCSN mechanism has been the difficulty to use observational data to place constraints on the details of the explosions.
%The reason for this are many-fold.
%The observed light curves and spectra are integral results of all the details of the explosions including, crucially, details of the progenitor such as mass loss history which have little impact on the details of the neutrino mechanism.
%Another issue is that the expense of detailed CCSN simulations is so great that generating large parameter studies that could be used in comparing to the population statistics of observed CCSN has, here-to-fore, not been possible.

I propose to carry out an extensive investigation of the sensitivity of the CCSN mechanism to underlying nuclear physics parameters.
I will construct a portable, flexible, open-source framework for conducting end-to-end parameter studies of the CCSN mechanism, beginning with nuclear physics inputs and ending with observable properties of simulated CCSN populations such as distributions of remnant masses, explosion energies, radioactive nickel production, neutrino signals, detailed nucleosynthetic yields, etc.
Distinct from other sensitivity studies \citep[e.g.,][]{Ugliano:2012, Perego:2015} that relied on ad hoc enhancement of neutrino heating to drive explosions, my work will directly account for role of {\it turbulence} in aiding CCSN explosions.
I have shown \citep{Couch:2015} that successful explosions in 2D and 3D do not absorb substantially more neutrino energy than comparable {\it failed} explosions in 1D.
Instead, I showed that the most important difference was the role of weakly compressible turbulence behind the stalled shock in 2D and 3D providing an effective pressure that aided multidimensional explosions.
Thus, successful multidimensional explosions are not really the result of additional, or more efficient, neutrino heating but instead the result of strong post-shock turbulence.
Turbulence is completely {\it missing} from 1D simulations and, crucially, driving 1D explosions with artificial neutrino heating does not accurately model the manner in which explosions develop in 3D.
This could invalidate predictions of CCSN observables based on previous 1D studies with artificial neutrino heating.

In order to better model the development of successful CCSN explosions in 1D, and avoid issues associated with artificially enhancing the neutrino heating, this work will utilize a model for the action of turbulence in 1D that I am currently developing.
My 1D turbulence model provides a physically-motivated estimate of the convective and turbulent speeds behind the shock, which then directly imply an effective turbulent pressure \citep{Radice:2015}.
The effective turbulent pressure will be included in the hydrodynamics where it will aid shock expansion, driving explosions without unphysically altering the thermodynamics by adding artificial neutrino heating.
I will use detailed 3D simulations to tune the free parameters in this model, such as those I am conducting as PI of the DOE INCITE project ``Petascale Simulation of Magnetorotational Core-collapse Supernovae.''
With this model for driving 1D explosions, I will be able to execute the most realistic 1D parameter studies of CCSNe yet, yielding invaluable connections between CCSN observables and the underlying nuclear physics at the heart of the explosion mechanism.

The framework I will develop for testing the dependence of the CCSN mechanism on input physics will be called the ``Supernova Sensitivity Machine'' (SSM).
SSM will consist of an open-source set of tools for: (1) reading and processing nuclear physics data such as weak rates, dense matter equation of state, neutrino cross sections, etc., into standardized formats ready for use in detailed CCSN simulations; (2) generating CCSN stellar progenitor models based on varied physics inputs such as key reaction rates; (3) executing parallel CCSN simulations using a sophisticated code including detailed neutrino transport; (4) processing CCSN simulation output through a large nuclear reaction network to determine detailed yields; (5) computing light curves and spectra from the CCSN simulation results; and (6) automatically generating key plots and diagrams based on the results of the CCSN simulations.
Using this set of tools, in combination with my model for turbulence in CCSNe, I will determine the nuclear physics parameters that CCSN observables are most sensitive to and how uncertainty in those physics parameters translate to uncertainty in our predictions of CCSN observables.
This work will provide valuable input into experimental efforts to constrain those nuclear physics parameters, such as those at the National Superconducting Cyclotron Laboratory and the forthcoming Facility for Rare Isotope Beams.

This work will tie together many separate efforts, yielding a much greater impact on the study of the CCSN mechanism than any of the individual efforts alone.
SSM will use emerging-standard nuclear physics data formats such as the NuLib neutrino opacity and equation of state tables\footnote{\href{https://github.com/evanoconnor/NuLib}{github.com/evanoconnor/NuLib}}, and the JINA REACLIB nuclear reaction rates library.  
SSM will utilize the open-source stellar evolution code MESA \citep{Paxton:2015}.  
I am already working closely with MESA developer F.X. Timmes in a separate effort to develop the most realistic CCSN progenitor models yet, both in 1D and 3D \citep{Couch:2015a}.
The primary CCSN simulation code for SSM will be FLASH \citep{Fryxell:2000}, which I have adapted to treat the physics of CCSNe.
The current version of the FLASH CCSN simulation application includes sophisticated, energy-dependent neutrino transport in an explicit two-moment approximation, a treatment of general relativistic effects in both gravity and neutrino transport, and multiple reaction networks for handling non-equilibrium composition at low density.
Initially, SSM will consider 1D CCSN simulations, though extension to 2D (and 3D) will be possible on current (and future) {\it Leadership}-class computing platforms.
Simulations in 1D with detailed transport using FLASH require only a few hours to complete on a single modern processor, making parameter studies consisting of hundreds of simulations feasible and quick to complete in a high-throughput computing environment.
SSM will utilize large nuclear networks of flexible size for computing detailed abundance yields.
Finally, SSM will generate bolometric and broadband light curves using SNEC \citep{Morozova:2015} and wavelength-dependent spectra using SuperNu \citep{Wollaeger:2013}.
I am a co-developer of both of these codes.
The synergy of all these distinct components for studying CCSNe will be made possible by my singular combination of expertise with each of them.
%Each module of SSM will be portable and interchangeable so that other investigators may use the open-source SSM with their choice of simulation and analysis codes.

%SSM will be used to measure the sensitivity of CCSN observables to the underlying nuclear physics in a statistically meaningful way by rigorously exploring the allowed parameter space using large numbers of explosion models.
%This, manifestly, requires successful explosions which are notoriously rare in self-consistent 1D simulations.
%Thus, we must resort to ad hoc means of initiating successful 1D explosions by enhancing the neutrino heating rates \citep[e.g.,][]{Ugliano:2012, Perego:2015}.
%Prediction of key CCSN observables, such as explosive nucleosynthetic yields, depend sensitively on how the explosions are initiated, however, raising the question of how accurately such artificial means of driving explosions compare to self-consistent multidimensional explosions. 
%Additionally,


The development of the SSM tools will be secondary to the science that they will enable.
The first target science deliverable of this work will be an investigation of what fraction of massive stars explode as supernovae and what the mass distributions of resulting remnants are.
This will be directly compared to observational data of neutron star and black hole masses.
This first effort will utilize the full range of progenitor masses from \citet{Woosley:2007d} combined with my model for CCSN turbulence to {\it press} 1D simulations to explosion without resorting to artificial neutrino heating.

%Making connection to observation is hard.  
%SSM will allow observational requirements to constrain explosion and progenitor models.

%Ran on HPCC.  
%Open-source, standard input data.  
%REACLIB \citep{Cyburt:2010}, NuLib, StellarCollapse EOS, FLASH.  
%Don't mention Skynet in pre-proposal.  
%Python for interface and scheduling.  

%Turbulent pressure term.  
%Approx GR gravity.  
%Full transport. 1 second of post-bounce time in 4.5 hours on one core (18 groups).
%Discuss long term.  Just mention.  NS cooling.

%Eventually: Multi-D, including 3D.

%Don't emphasize the toy too much.  
%The point is the science this will enable!  
%Ties together separate efforts that have been separately funded.  
%Now we can put it all together!

%Data emulation.  
%Bayesian statistics.  
%Uncertainty quantification!  
%Reproducibility!
%Interoperability with other codes/models.
%What impact to the uncertainties in fundamental quantities have on global, population statistics of SNe?  
%And does this impact, e.g., galactic cooling models.  
%Mention O'Shea.
%INCITE project.  Will be used to validate 1D model.  Eventually, we will do this in 2D/3D.

%CCSNe are snowflakes.  
%Trying to compare to individual objects and then learn something about the physics of the mechanism is doomed to failure. 
%We must compare to the ensemble of the population, in some way.

%{\it Push} vs. {\it Press}: my new approach to 1D explosions.

%FRIB.



%No calibration needed.

%\renewcommand\bibsection{\section*{References}}
\setlength{\bibsep}{2pt}
\begin{multicols}{2}
\bibliography{DOE_earlyCareer_pre_couch.bib}
\end{multicols}



\newpage

\noindent {\bf Collaborators and Co-editors} (Total: 21): \\
A. Arcones (Technische Universit\"at Darmstadt),\\
W.D. Arnett (University of Arizona),\\
E.P. O'Connor (Canadian Institute for Theoretical Astrophysics),\\
A. Dubey (Lawrence Berkeley National Lab),\\
N. Flocke (University of Chicago), \\
C. Fr\"ohlich (North Carolina State University),\\
C. Graziani (University of Chicago), \\
D.Q. Lamb (University of Chicago),\\
D. Lazzati (Oregon State University),\\
D. Lee (University of California Santa Cruz),\\
M. Milosavljevi\'c (University of Texas at Austin),\\
G.A. Moses (University of Wisconsin),\\
C.D. Ott (California Institute of Technology), \\
D. Pooley (Sam Houston State University),\\
D. Radice (California Institute of Technology), \\
D.R. van Rossum (University of Chicago),\\
T.A. Thompson (Ohio State University),\\
F.X. Timmes (Arizona State University),\\
P. Tzeferacos (University of Chicago), \\
J.C. Wheeler (University of Texas at Austin), \\
R.T. Wollaeger (University of Chicago/LANL) \\


\vskip.1cm
\noindent {\bf Graduate Advisors and Postdoctoral Sponsors} (Total: 4): \\
D.Q. Lamb (Chicago),\\
M. Milosavljevi\'c (Texas; PhD co-advisor),\\
C.D. Ott (Caltech),\\
J.C. Wheeler (Texas, PhD advisor)\\

\vskip.1cm
\noindent {\bf Graduate Advisees:} (Total: 1)\\
C. Mattes (Darmstadt)

\end{document}