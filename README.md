Proposal to DOE Early Career Program.
======
http://science.energy.gov/early-career/

Title: The SuperNova Sensitivity Machine (SNSM)

Overarching goal:
Create a tool for quickly, easily, and reliably testing the sensitivity
of CCSN simulations to nuclear physics inputs, such as EOS, nuclear rxn
rates, neutrino cross sections, etc.

The SNSM will at first consist of 1D simulations using various approximations.
Python will be used to build the machine and web-based interface will be
available.  The interface will run primarily on my group's server, and
the simulations will be carried out on the MSU HPCC.

Python tools will be developed for translating EOS/neutrino opacity tables
into standard format.  We will use the stellarcollapse EOS and NuLib.

At first, at least, FLASH will be pre-compiled.
