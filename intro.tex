\section{Overview and Objectives}

Core-collapse supernovae (CCSNe) are the most extreme laboratories for nuclear physics in the universe.
Stellar core collapse and the violent explosions that follow give birth to neutron stars and black holes, and in the process synthesize most of the elements heavier than helium throughout the universe.
The behavior of matter at supranuclear densities is crucial to the CCSN mechanism, as are strong and weak interactions.
Beyond Standard Model behavior of neutrinos may also impact the CCSN mechanism.
Despite the key role CCSNe play in many aspects of astrophysics, and decades of research effort, {\it we still do not understand the details of the physical mechanism that causes these explosions.}
This leaves frustratingly large error bars on many key aspects of our theoretical understanding of the universe, and also makes it difficult to constrain uncertain nuclear physics with data from CCSNe.

I propose to carry out an extensive investigation of the sensitivity of the CCSN mechanism to underlying nuclear physics parameters.
I will construct a portable, flexible, open-source framework for conducting end-to-end parameter studies of the CCSN mechanism, beginning with nuclear physics inputs and ending with observable properties of simulated CCSN populations such as distributions of remnant masses, explosion energies, radioactive nickel production, neutrino signals, detailed nucleosynthetic yields, etc.
Distinct from other sensitivity studies \citep[e.g.,][]{Ugliano:2012, Perego:2015} that relied on ad hoc enhancement of neutrino heating to drive explosions, my work will directly account for role of {\it turbulence} in aiding CCSN explosions.
I have shown \citep{Couch:2015} that successful explosions in 2D and 3D do not absorb substantially more neutrino energy than comparable {\it failed} explosions in 1D.
Instead, I showed that the most important difference was the role of weakly compressible turbulence behind the stalled shock in 2D and 3D providing an effective pressure that aided multidimensional explosions.
Thus, successful multidimensional explosions are not really the result of additional, or more efficient, neutrino heating but instead the result of strong post-shock turbulence.
Turbulence is completely {\it missing} from 1D simulations and, crucially, driving 1D explosions with artificial neutrino heating does not accurately model the manner in which explosions develop in 3D.
This could invalidate predictions of CCSN observables based on previous 1D studies with artificial neutrino heating.




In order to better model the development of successful CCSN explosions in 1D, and avoid issues associated with artificially enhancing the neutrino heating, this work will utilize a model for the action of turbulence in 1D that I am currently developing.
My 1D turbulence model provides a physically-motivated estimate of the convective and turbulent speeds behind the shock, which then directly imply an effective turbulent pressure \citep{Radice:2015}.
The effective turbulent pressure will be included in the hydrodynamics where it will aid shock expansion, driving explosions without altering the thermodynamics by adding artificial neutrino heating.
I will use detailed 3D simulations to tune the free parameters in this model, such as those I am conducting as PI of the DOE INCITE project ``Petascale Simulation of Magnetorotational Core-collapse Supernovae.''
With this model for driving 1D explosions, I will be able to execute the most realistic 1D parameter studies of CCSNe yet, yielding invaluable connections between CCSN observables and the underlying nuclear physics at the heart of the explosion mechanism.