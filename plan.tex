\section{Research Plan}

This project will be a comprehensive study of the role of turbulence in aiding neutrino-driven CCSN explosions.
This effort will comprise two interconnected tracts of research: 1. state-of-the-art 3D simulations of turbulence in CCSN with world-leading resolution and physical fidelity; and 2. sensitivity studies of CCSN observables to input nuclear physics using a physically-motivated 1D model for turbulent pressure.
The high-resolution 3D simulations carried out in this project will be used to tune the 1D turbulent pressure model and the 1D model will be used to carry out extensive parameter studies of CCSN.
These studies will be used to directly quantify the sensitivity of CCSN observables, such as remnant masses, explosion energies, nickel production, neutrino signals, etc., to input physics such as neutrino interactions and cross sections, nuclear reaction rates, and equations of state.
This project will compliment current DOE-supported research exploring the nature of nuclear matter, neutrino physics, heavy element nucleosynthesis, and neutron stars.
This project will explore the sensitivity of the CCSN mechanism and observables such as detailed nucleosynthetic yields to underlying nuclear physics parameters, data
that will aid in targeting experimental efforts at, e.g., RHIC, JLAB, and FRIB.

\subsection{High-fidelity 3D Simulations of CCSN}
\label{sec:incite}

This project will directly support the execution of the on-going DOE INCITE award ``Petascale Simulation of Magnetorotational Core-collapse Supernovae'' for which I am PI.
This award provides 100 million core-hours per year for 2016 and 2017 on Argonne Leadership Computing Facility's 10-petaflop {\it Mira} platform.
This INCITE allocation will be used to execute a world-leading study of the role of turbulence in CCSNe.
This includes pure-hydrodynamic turbulence and magnetorotational turbulence.
The Leadership-class resources provided will be used to carry out the highest-resolution CCSN simulations with detailed physics yet.
In addition to the extremely high-resolution simulations I will direct as part of this project, I am currently leading an effort to simulate 3D CCSN with fully-multidimensional neutrino transport in an explicit two-moment approximation.


With support from the DOE Early Career Research program, I will continue this cutting edge numerical study of CCSN turbulence.
Extremely-high resolution simulations capable of capturing the correct behavior of turbulence behind the CCSN shock will be made tractable by using an approximate neutrino tranport scheme.
These simulations will utilize the efficient and physically-detailed neutrino ``leakage'' scheme described in \citet{OConnor:2010}, extended to 3D in the `ray-by-ray' approximation \citep{Couch:2014}.  
The leakage of neutrinos is computed along radial rays and neutrino source terms are computed locally following interpolation of the radiation field quantities back to the Cartesian hydro grid.  
Additionally, I will use FLASH's highly flexible adaptive mesh refinement package to place high resolution elements only where the medium is turbulent: in the gain region.
This will avoid having very high resolution covering the neutron star which would drive the allowed time step down dramatically.  
An example of my meshing scheme is shown in Figure \ref{fig:amr}.
This work will be an extension of the high-resolution 3D simulations of \citet{Radice:2015a} to realistic simulations of the CCSN mechanism including all the required physics.


This project will also explore magnetorotational turbulence in the CCSN context.
One of the primary goals of my INCITE project is to directly simulate the growth of the magnetorotational instability (MRI) in the gain region of a CCSN simulation.
Results from this effort have already confirmed that the conditions behind the stalled shock are susceptible to growth of the MRI even for mildly rotating and weakly magnetic progenitor stars.
Capturing the growth of the MRI in the CCSN gain region requires resolution elements of $\Delta x \lesssim 125$ km, roughly equivalent to what we estimated was necessary to directly simulate hydrodynamic turbulence in the gain layer \citep*{Radice:2015}.
These simulation could potential be used to validate a sub-grid ``large-eddy'' model for CCSN turbulence that could be used in low-resolution CCSN simulations, though such work is beyond the scope of this project.


Additionally I will carry out 3D CCSN simulations employing full multi-dimensional, energy-dependent neutrino transport.  
These simulations will use the ``M1'' transport scheme of \citet{OConnor:2015, OConnor:2015a}.  
M1 computes the first two moments (the zeroth and first, hence ``M1'') of the Boltzmann equation for neutrinos with an analytic closure for the higher-order moments.  
The neutrino fluxes are computed explicitly as a hyperbolic system resulting in favorable performance and scaling properties (at the cost of time step sizes limited by the speed of light) while the matter-radiation source terms are computed implicitly.  
This implicit solve is completely local and requires solving only a 4x4 matrix.

The M1 approach, thanks to solving for an additional, higher-order moment, is inherently more accurate than zeroth-moment only approaches such as flux-limited diffusion \citep[e.g.,][]{Bruenn:2013, {Dolence:2015}}.
M1 does not require a flux-limiter-based closure for the radiation fluxes as they are solved for directly.
As compared with flux-limited diffusion, M1 does not suffer from the inability to capture ``shadows'' inherent to FLD schemes \citep{OConnor:2015a}.
The severe limitation of time steps determined by the speed of light is not so drastic in CCSNe since the explicit time step is already just a factor of a few larger than this thanks to the enormous sound speeds in the PNS.
Another significant advantage of M1 is that it is a fully multidimensional transport scheme, i.e., the solution at a given grid point is dependent on the fluxes from every direction around that point.  
This is distinct from the often-adopted ``ray-by-ray'' approximation \citep[e.g.,][]{Bruenn:2013, {Muller:2012}, {Hanke:2013}} in which the transport problem is solved only along discrete radial rays.

\begin{wrapfigure}{r}{3.1in}
  \includegraphics[width=3.1in]{figs/plasmaBeta_o110b9_125mV500m_grid_0393_cropped.png}
  \caption{Comparison of AMR grid structures from a 3D CCSN simulation with 0.5$^\circ$ resolution (left) and the high resolution simulation proposed here with 0.1$^\circ$ resolution (right).  Shown is the plasma beta parameter from a simulation including rotation and magnetic fields.}
  \label{fig:amr}
\end{wrapfigure}
As part of this project, we will carry out a series of 3D CCSN simulations using multidimensional M1 transport.
We will use truly multidimensional progenitor models that I will produce \citep{Couch:2015a}.
These simulations will test whether the presence of strong non-spherical motion driven by the late stages of convection and nuclear burning in the progenitor can trigger successful turbulence-aided neutrino-driven explosions \citep[e.g.][]{Couch:2013b, Couch:2015, Muller:2015}.

This project will support a full time postdoctoral research associate who will lead the execution of the 3D CCSN simulations under my supervision.


\subsection{A Model for Turbulent Pressure in 1D CCSN Simulations}
\label{sec:model}

Much recent work has shown that the effective turbulent pressure behind the stalled CCSN shock is dynamically important to aiding neutrinos in driving successful explosions \citep{Murphy:2013, Couch:2013b, Couch:2015, Couch:2015a, Radice:2015a}.
I have shown \citep{Couch:2015} that successful explosions in 2D and 3D do not absorb substantially more neutrino energy than comparable {\it failed} explosions in 1D.
Instead, I showed that the most important difference was the role of weakly compressible turbulence behind the stalled shock in 2D and 3D providing an effective pressure that aided multidimensional explosions.
Thus, successful multidimensional explosions are not really the result of additional, or more efficient, neutrino heating but instead the result of strong post-shock turbulence.
Turbulence is completely {\it missing} from 1D simulations and, crucially, driving 1D explosions with artificial neutrino heating does not accurately model the manner in which explosions develop in 3D.
This could invalidate predictions of CCSN observables based on previous 1D studies with artificial neutrino heating.

As part of this project, I will develop an algorithm for accounting for the action of turbulence in 1D simulations.
Turbulence can be described using a standard Reynolds decomposition \citep[e.g.,][]{Murphy:2011a} where the Reynolds stress tensor is
\begin{equation}
R_{ij} = v^\prime_i v^\prime_j \approx \frac{\langle \rho u_i v^\prime_j \rangle}{\rho_0},
\end{equation}
where $v_j^\prime = u_j - v_j$, $u_j = \langle v_j \rangle$, and $\rho_0 = \langle \rho \rangle$.  
Averages, $\langle ... \rangle$, are taking over spherical solid angle.
The turbulence in the CCSN mechanism is strongly anisotropic, being roughly twice as strong in the radial direction as in either angular direction, i.e, $v^{\prime 2}_r \sim v^{\prime 2}_\theta + v^{\prime 2}_\phi$.
We can thus approximate the turbulent pressure as
\begin{equation}
P_{\rm turb} \approx \rho_0 R_{rr} = \rho_0 v^{\prime 2}_r.
\label{eq:pturb}
\end{equation}
The anisotropy of the turbulence in the CCSN gain region has another important implication.
Equating Eq. (\ref{eq:pturb}) to a gamma-law equation of state, $(\gamma_{\rm turb}-1) \rho_0 e_{\rm turb}$, and taking $e_{\rm turb} = 0.5 v^{\prime 2} \sim v^{\prime 2}_r$, as implied by the anisotropy, we find that $\gamma_{\rm turb} \approx 2$.
This is far {\it stiffer} than the thermal equation of state which has $\gamma_{\rm th} \approx 4/3$, meaning that per unit specific internal energy, turbulence exerts a far greater pressure than the thermal state of the gas behind the CCSN shock.
This is why turbulence is so effective at aiding the neutrinos in pushing the shock out.

Here, we seek a method for incorporating the influence of the turbulent pressure in 1D simulations of the CCSN mechanism.
The turbulence in the gain region is the result of neutrino-driven convection, hence we will resort to the mixing-length theory (MLT) to describe convection in 1D \citep[e.g.][]{Cox:1968}.
MLT has a long history of use in stellar modeling and evolution and can successful reproduce many observed properties of the solar photosphere \citep{Asplund:2009}.
My MLT formalism follows \citet{Wilson:1988} who incorporated an MLT algorithm for mixing into their 1D CCSN simulations.
The basic concept of MLT is that, in the presence of a convectively unstable stratification, a buoyant blob will rise through some distance $\ell_{\rm MLT}$ until it reaches pressure equilibrium with the surrounding medium.
In this process, buoyant blobs transport energy and mix the composition in convective layers.
$\ell_{\rm MLT}$ is the so-called mixing length and is typical some fraction, $\alpha_{\rm MLT}$, of the local pressure scale height, $\ell_{\rm MLT} = \alpha_{\rm MLT} P (dP/dr)^{-1}$.
Here, I use the Ledoux criterion for convection which accounts for both entropy and composition gradients,
\begin{equation}
C_{\rm Ledoux} = \frac{d\rho}{dr} - \frac{1}{c_s^2} \frac{dP}{dr},
\end{equation}
where $c_s$ is the adiabatic sound speed and $P$ is the thermal pressure.
In general, convection occurs for $C_{\rm Ledoux} > 0$.
A rising MLT blob will experience a change in density of
\begin{equation}
\Delta \rho = \ell_{\rm MLT} C_{\rm Ledoux}.
\label{eq:drho}
\end{equation}
Requiring that a blob conserve energy during its ascent implies that it will attain a speed $v_{\rm MLT}$ defined by,
\begin{equation}
\frac{1}{2} \rho v_{\rm MLT} = g \Delta \rho \ell_{\rm MLT},
\end{equation}
where $g = -d\Phi/dr$ is the local gravitational acceleration.
Solving for the speed,
\begin{equation}
v_{\rm MLT} = \sqrt{2 g \frac{\Delta \rho}{\rho} \ell_{\rm MLT}} = \sqrt{\frac{2 g}{\rho} C_{\rm Ledoux} \ell_{\rm MLT}^2}, 
\end{equation}
where I have used Eq. (\ref{eq:drho}) for the density change.
In order to relate the MLT view of convection to the turbulence in the CCSN gain region, I assume the radial turbulent speed is
\begin{equation}
v^{\prime}_r = v_{\rm turb} = \beta_{\rm turb} v_{\rm MLT},
\label{eq:vturb}
\end{equation}
where $\beta_{\rm MLT}$ is a tuning parameter of order unity.
The turbulent pressure is then simply $P_{\rm turb} = \rho v_{\rm turb}^2$.
This model for the turbulent pressure can be evaluated completely locally and has only two free parameters: $\alpha_{\rm MLT}$ and $\beta_{\rm turb}$.
These parameters will be set by directly comparing to high-resolution 3D simulations of neutrino-driven convection in CCSNe.

\begin{wrapfigure}{l}{3.2in}
  \includegraphics[width=3.2in]{figs/mlt_turb.pdf}
  \caption{Turbulent speeds from a 3D CCSN simulation (red line) along with turbulent speeds based on the model proposed here for the turbulent pressure.  
  The blue line shows the turbulent speed for a 1D CCSN simulation while the green line shows the estimate based on the angle-average background of the 3D simulation.  The fit parameters used here are $\alpha_{\rm MLT} = 1.8$ and $\beta_{\rm turb}=0.4$ which gives a reasonable fit to actual 3D data. 
This model will be used to drive explosions in 1D simulations in a far more realistic way than enhancing the neutrino heating rates.}
  \label{fig:mlt}
\end{wrapfigure}
In order to account for the action of the turbulent pressure in 1D simulations of CCSNe, I will modify the hydrodynamic equations to include the turbulent pressure directly.
In 1D spherical symmetry, the modified momentum and energy equations read,
\begin{equation}
\frac{\partial \rho v}{\partial t} + \frac{1}{r^2} \frac{\partial}{\partial r} (r^2 \rho v^2) + \frac{\partial P_{\rm tot}}{\partial r} = -\rho \frac{\partial \Phi}{\partial r},
\end{equation}
and
\begin{equation}
\frac{\partial e_{\rm tot}}{\partial t} + \frac{1}{r^2}\frac{\partial}{\partial r} [r^2 v (e_{\rm tot} + P_{\rm tot})] = -\rho v \frac{\partial \Phi}{\partial r} + Q_{\nu},
\end{equation}
where $P_{\rm tot} = P_{\rm th} + P_{\rm turb}$ is the total pressure and $e_{\rm tot} = 0.5 \rho (v^2 + v_{\rm turb}^2) + \epsilon$ is the total energy.
$Q_{\nu}$ is the net energy neutrino source term and $\epsilon$ is the internal energy density.
The hydrodynamics solver in FLASH will be modified to include these terms accounting for turbulent motion appropriately, including correct coupling to the Riemann solver during flux calculation.


In Figure \ref{fig:mlt} I show an example of the turbulent velocity model given by Eq. (\ref{eq:vturb}).
This figure shows the model turbulent speed computed from a 1D CCSN explosion simulation (blue line) and a 3D explosion model (green line) compared to the actual measured turbulent speed, $v^{\prime}_r$, from the same 3D simulation (red line).
These are the same 1D and 3D simulations shown in Figure \ref{fig:entrYe}.
The model parameters for the 1D fit are $\alpha_{\rm MLT} = 1.8$ and $\beta_{\rm turb} = 0.4$.
As seen, for these parameters the turbulent model described by Eq. (\ref{eq:vturb}) gives good agreement with the actual 3D simulation.
And this is for a very simple model based only on MLT without any other fixes common to stellar convection modeling such as convective overshoot or semi-convection.
This is merely a heuristic example that a model for the turbulent pressure based on estimates from MLT has the potential to accurately describe realistic 3D simulations.
As part of this project, the model parameters will be tuned based on the entire time evolution of 3D simulations so that the 1D model reproduces closely the entire history of a 3D simulation, rather than simply a snap shot as is done in Figure \ref{fig:mlt}.

In this project, I will implement this turbulent pressure model for 1D simulations in FLASH.  
This model will be used to artificially drive 1D explosions in a manner that more faithfully reproduces the behavior of 3D explosion simulations.
The addition of the free parameter $\beta_{\rm turb}$ will allow for tuning the explosions to yield particular explosions energies as is done in, e.g., \citet{Ugliano:2012, Sukhbold:2015, Perego:2015}.
These model parameters will be determined using the high-resolution 3D simulations of neutrino-driven CCSNe carried out as part of this project (Section \ref{sec:incite}).
I will also include the standard MLT approach to computing convective energy fluxes and compositional mixing \citep[e.g.,][]{Wilson:1988}.
The turbulent pressure resulting from this model will aid the neutrino heating in driving successful 1D explosions that, in average quantities, more closely resemble 3D explosions than would otherwise be the case if the neutrino heating were enhanced in some manner to yield explosion.

\subsection{Quantifying Uncertainties in CCSN Models: The Supernova Sensitivity Machine}

Using the new method of driving 1D explosions via the addition of a turbulent pressure, 
I will carry out extensive sensitivity studies of the CCSN mechanism to underlying nuclear physics parameters.
Sensitivity studies, while common in nuclear physics, are more rare in the study of the CCSN mechanism \citep[but see][]{Lentz:2012, Lentz:2012a, Sullivan:2015}.
This is due both to the expense of high-fidelity CCSN simulations and the lack of an iron-clad model for the explosion mechanism.
The model for CCSN explosions discussed in this proposal, which could be called the turbulence-aided neutrino heating mechanism, provides a theory for explosions that can be tested against observations.
And modern computing power makes high-fidelity 1D simulations with realistic neutrino transport cheap.



In order to facilitate large parameter and sensitivity studies of CCSNe, I will construct a portable, flexible, open-source framework for conducting end-to-end simulation campaigns of the CCSN mechanism, beginning with nuclear physics inputs and ending with observable properties of simulated CCSN populations such as distributions of remnant masses, explosion energies, radioactive nickel production, neutrino signals, detailed nucleosynthetic yields, etc.
Distinct from other sensitivity studies \citep[e.g.,][]{Ugliano:2012, Perego:2015} that relied on ad hoc enhancement of neutrino heating to drive explosions, my work will directly account for role of {\it turbulence} in aiding CCSN explosions via the method described in Section \ref{sec:model}.


My 1D turbulence model provides a physically-motivated estimate of the convective and turbulent speeds behind the shock, which then directly imply an effective turbulent pressure \citep{Radice:2015}.
The effective turbulent pressure will be included in the hydrodynamics where it will aid shock expansion, driving explosions without unphysically altering the thermodynamics by adding artificial neutrino heating.
I will use detailed 3D simulations to tune the free parameters in this model, such as those I am conducting as PI of a DOE INCITE project (Section \ref{sec:incite}).
With this model for driving 1D explosions, I will be able to execute the most realistic 1D parameter studies of CCSNe yet, yielding invaluable connections between CCSN observables and the underlying nuclear physics at the heart of the explosion mechanism.

\begin{wrapfigure}{r}{3.5in}
  \includegraphics[width=3.5in]{figs/SSM_diagram.pdf}
  \caption{Schematic of the Supernova Sensitivity Machine.} 
  \label{fig:ssm}
\end{wrapfigure}
The framework I will develop for testing the dependence of the CCSN mechanism on input physics will be called the ``Supernova Sensitivity Machine'' (SSM).
SSM will consist of an open-source set of tools for: (1) reading and processing nuclear physics data such as weak rates, dense matter equation of state, neutrino cross sections, etc., into standardized formats ready for use in detailed CCSN simulations; (2) generating CCSN stellar progenitor models based on varied physics inputs such as key reaction rates; (3) executing parallel CCSN simulations using a sophisticated code including detailed neutrino transport; (4) processing CCSN simulation output through a large nuclear reaction network to determine detailed yields; (5) computing light curves and spectra from the CCSN simulation results; and (6) automatically generating key plots and diagrams based on the results of the CCSN simulations.
Using this set of tools, in combination with my model for turbulence in CCSNe, I will determine the nuclear physics parameters that CCSN observables are most sensitive to and how uncertainty in those physics parameters translate to uncertainty in our predictions of CCSN observables.
This work will provide valuable input into experimental efforts to constrain those nuclear physics parameters, such as those at the National Superconducting Cyclotron Laboratory and the forthcoming Facility for Rare Isotope Beams.
A schematic diagram of the various SSM components and products is shown in Figure \ref{fig:ssm}.


This work will tie together many separate efforts, yielding a much greater impact on the study of the CCSN mechanism than any of the individual efforts alone.
SSM will use emerging-standard nuclear physics data formats such as the NuLib neutrino opacity and equation of state tables\footnote{\href{https://github.com/evanoconnor/NuLib}{github.com/evanoconnor/NuLib}}, and the JINA REACLIB nuclear reaction rates library.  
SSM will utilize the open-source stellar evolution code MESA \citep{Paxton:2015}.  
I am already working closely with MESA developer F.X. Timmes and W.D. Arnett in a separate effort to develop the most realistic CCSN progenitor models yet, both in 1D and 3D \citep{Couch:2015a}.
The primary CCSN simulation code for SSM will be FLASH \citep{Fryxell:2000}, which I have adapted to treat the physics of CCSNe.
The current version of the FLASH CCSN simulation application includes sophisticated, energy-dependent neutrino transport in an explicit two-moment approximation \citep{OConnor:2015}, a treatment of general relativistic effects in both gravity and neutrino transport \citep{OConnor:2015a}, and multiple reaction networks for handling non-equilibrium composition at low density \citep{Couch:2015a}.
Initially, SSM will consider 1D CCSN simulations, though extension to 2D (and 3D) will be possible on current (and future) {\it Leadership}-class computing platforms.
Simulations in 1D with detailed transport using FLASH require only a few hours to complete on a single modern processor, making parameter studies consisting of hundreds of simulations feasible and quick to complete in a high-throughput computing environment.
SSM will utilize large nuclear networks of flexible size for computing detailed abundance yields.
Finally, SSM will generate bolometric and broadband light curves using SNEC \citep{Morozova:2015} and wavelength-dependent spectra using SuperNu \citep{Wollaeger:2013}.
I am a co-developer of both of these codes.
The synergy of all these distinct components for studying CCSNe will be made possible by my singular combination of expertise with each of them.
Each module of SSM will be portable and interchangeable so that other investigators may use the open-source SSM with their choice of simulation and analysis codes.

%SSM will be used to measure the sensitivity of CCSN observables to the underlying nuclear physics in a statistically meaningful way by rigorously exploring the allowed parameter space using large numbers of explosion models.
%This, manifestly, requires successful explosions which are notoriously rare in self-consistent 1D simulations.
%Thus, we must resort to ad hoc means of initiating successful 1D explosions by enhancing the neutrino heating rates \citep[e.g.,][]{Ugliano:2012, Perego:2015}.
%Prediction of key CCSN observables, such as explosive nucleosynthetic yields, depend sensitively on how the explosions are initiated, however, raising the question of how accurately such artificial means of driving explosions compare to self-consistent multidimensional explosions. 
%Additionally,


The development of the SSM tools will be secondary to the science that they will enable.
The first target science deliverable of this work will be an investigation of what fraction of massive stars explode as supernovae and what the mass distributions of resulting remnants are.
This will be directly compared to observational data of neutron star and black hole masses.
This first effort will utilize the full range of progenitor masses from \citet{Woosley:2007d} combined with my model for CCSN turbulence to {\it press} 1D simulations to explosion without resorting to artificial neutrino heating.
The first nuclear physics dependency I will test the sensitivity to is the equation of state.
This work will utilize the current most commonly used equation of state in CCSN simulations, the model of \citet{Lattimer:1991} with nuclear incrompressibility $K_0 = 220$ MeV.
We will also use the equations of state by \citet{Steiner:2013} based on fits to observational data of neutron stars, as well as that of \citet{Hempel:2012} and others.
Tables for these equations of state, along with many others, in FLASH-compatible format are already available.\footnote{\url{stellarcollapse.org/equationofstate}}
This work will directly connect, in statistically meaningful way, key CCSN model observables (fraction of explosions, remnant mass distributions) to a key nuclear physics input (the equation of state).

SSM will also be used to test the sensitivity of CCSN observables to neutrino physics, in a manner akin to that presented by \citet{Lentz:2012, Lentz:2012a}.
With my model for the turbulent pressure in 1D, however, I will be able to study the sensitivity of CCSN observables to input neutrino physics rather than merely the pre-explosion phase behavior.
I will test the sensitivity of CCSN observables to various aspects of the include neutrino physics, such as inelastic scattering, weak magnetism corrections, update cross sections and rates, etc., in a systematic way.

SSM will also include the ability to explore sensitivity to progenitor model.
As my collaboration with F.X. Timmes and W.D. Arnett continues, we will produce new sets of state-of-the-art 1D CCSN progenitors using MESA.
I will test these new stellar models using SSM and compare the resulting CCSN observables to those obtained with the progenitor models of \citet{Woosley:2007d}.
Stellar evolution is {\it not} a solved problem, and this is particularly true for high mass stars, so this effort will be incredibly informative.
I will also use MESA to generate new sets of progenitor models with varied physics input, in particular nuclear reaction rates.
One of the first such reaction rates I will explore in this way is C$^{12}(\alpha,\gamma)$O$^{16}$.
This reaction rate is fairly uncertain, but plays a critical role in high mass stellar evolution \citep[e.g.,][]{Austin:2014}.

As SSM develops, I will extend the CCSN observables produced to include detailed nucleosynthetic results.
This work will address whether CCSNe, or a subset of CCSNe, are capable of producing r-process elements.
Electromagnetic light curves and spectra will also be produced using SNEC \citep{Morozova:2015} and SuperNu \citep{Wollaeger:2013}.
The will put astronomical observations in direct contact with my model for the CCSN mechanism and, through the systematic sensitivity studies I will conduct, with input nuclear physics.
By determining the sensitivity of the CCSN mechanism and observables to nuclear physics parameters, this project will directly aid in the design and execution of experimental efforts at, e.g., NSCL, FRIB, JLAB, and RHIC.

This project will support a graduate student who I will supervise that will assist myself and the project postdoc in construction of the SSM and execution of sensitivity studies using FLASH and MESA.
I expect this work will be the students PhD thesis topic.


